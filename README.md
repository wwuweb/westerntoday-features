# **WesternToday Export**

## **Feature configuration**

1. Featured stories

    1. Contains the large picture header view and the next 4 featured stories

    2. There was an issue with an image style not being included, but my last commit should have fixed it.  This one should work automatically.

2. MailChimp

    3. Contains 

        1. MailChimp block

    4. To do

        2. Create a block with the newsletter image and add it to the panel below the MailChimp block

3. Western TV

    5. Contains

        3. WesternTV content type

        4. First story preview

        5. Story list views

    6. To Do

        6. Should work out of the box

4. Feeds

    7. Contains

        7. All 4 bottom feeds

        8. New stories

    8. To do

        9. Popular feed didn’t want to be included.  Needs to be added to panel and configured to match the number of posts the "new" feed has.  To get the button to match the other feed, remove the global text field and add a “more” link.