<?php
/**
 * @file
 * featured_stories_views.features.inc
 */

/**
 * Implements hook_views_api().
 */
function featured_stories_views_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function featured_stories_views_image_default_styles() {
  $styles = array();

  // Exported image style: featured_story_preview.
  $styles['featured_story_preview'] = array(
    'label' => 'Featured Story Preview',
    'effects' => array(
      10 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 250,
          'height' => 250,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: first_featured_story.
  $styles['first_featured_story'] = array(
    'label' => 'First Featured Story',
    'effects' => array(
      11 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 1080,
          'height' => 450,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}
