<?php
/**
 * @file
 * featured_stories_views.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function featured_stories_views_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'features';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Features';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['css_class'] = 'first-featured-story';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_type'] = 'h2';
  $handler->display->display_options['fields']['title']['element_class'] = 'helper-clear-float';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_western_today_image']['id'] = 'field_western_today_image';
  $handler->display->display_options['fields']['field_western_today_image']['table'] = 'field_data_field_western_today_image';
  $handler->display->display_options['fields']['field_western_today_image']['field'] = 'field_western_today_image';
  $handler->display->display_options['fields']['field_western_today_image']['label'] = '';
  $handler->display->display_options['fields']['field_western_today_image']['element_class'] = 'float-right';
  $handler->display->display_options['fields']['field_western_today_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_western_today_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_western_today_image']['type'] = 'file_rendered';
  $handler->display->display_options['fields']['field_western_today_image']['settings'] = array(
    'file_view_mode' => 'teaser',
  );
  $handler->display->display_options['fields']['field_western_today_image']['delta_limit'] = '1';
  $handler->display->display_options['fields']['field_western_today_image']['delta_offset'] = '0';
  /* Field: Content: Video */
  $handler->display->display_options['fields']['field_westerntv_video']['id'] = 'field_westerntv_video';
  $handler->display->display_options['fields']['field_westerntv_video']['table'] = 'field_data_field_westerntv_video';
  $handler->display->display_options['fields']['field_westerntv_video']['field'] = 'field_westerntv_video';
  $handler->display->display_options['fields']['field_westerntv_video']['label'] = '';
  $handler->display->display_options['fields']['field_westerntv_video']['element_class'] = 'float-right';
  $handler->display->display_options['fields']['field_westerntv_video']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_westerntv_video']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_westerntv_video']['type'] = 'file_rendered';
  $handler->display->display_options['fields']['field_westerntv_video']['settings'] = array(
    'file_view_mode' => 'preview',
  );
  $handler->display->display_options['fields']['field_westerntv_video']['delta_offset'] = '0';
  /* Field: Content: Post date */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'node';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['label'] = 'Published';
  $handler->display->display_options['fields']['created']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['created']['date_format'] = 'long';
  $handler->display->display_options['fields']['created']['second_date_format'] = 'long';
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['type'] = 'text_trimmed';
  $handler->display->display_options['fields']['body']['settings'] = array(
    'trim_length' => '600',
  );
  /* Field: Content: All taxonomy terms */
  $handler->display->display_options['fields']['term_node_tid']['id'] = 'term_node_tid';
  $handler->display->display_options['fields']['term_node_tid']['table'] = 'node';
  $handler->display->display_options['fields']['term_node_tid']['field'] = 'term_node_tid';
  $handler->display->display_options['fields']['term_node_tid']['label'] = '';
  $handler->display->display_options['fields']['term_node_tid']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['term_node_tid']['vocabularies'] = array(
    'annual_report' => 0,
    'brand_characteristics' => 0,
    'colleges' => 0,
    'media_folders' => 0,
    'people' => 0,
    'topics' => 0,
  );
  /* Sort criterion: Content: Sticky */
  $handler->display->display_options['sorts']['sticky']['id'] = 'sticky';
  $handler->display->display_options['sorts']['sticky']['table'] = 'node';
  $handler->display->display_options['sorts']['sticky']['field'] = 'sticky';
  $handler->display->display_options['sorts']['sticky']['order'] = 'DESC';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'profile' => 'profile',
    'feature' => 'feature',
    'photo_collection' => 'photo_collection',
    'video' => 'video',
  );
  /* Filter criterion: Content: Promoted to front page */
  $handler->display->display_options['filters']['promote']['id'] = 'promote';
  $handler->display->display_options['filters']['promote']['table'] = 'node';
  $handler->display->display_options['filters']['promote']['field'] = 'promote';
  $handler->display->display_options['filters']['promote']['value'] = '1';

  /* Display: Home Page Slideshow */
  $handler = $view->new_display('panel_pane', 'Home Page Slideshow', 'panel_pane_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '7';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'flexslider';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['label'] = '';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  /* Field: Content: Path */
  $handler->display->display_options['fields']['path']['id'] = 'path';
  $handler->display->display_options['fields']['path']['table'] = 'node';
  $handler->display->display_options['fields']['path']['field'] = 'path';
  $handler->display->display_options['fields']['path']['label'] = '';
  $handler->display->display_options['fields']['path']['exclude'] = TRUE;
  $handler->display->display_options['fields']['path']['element_label_colon'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_type'] = 'h2';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_western_today_image']['id'] = 'field_western_today_image';
  $handler->display->display_options['fields']['field_western_today_image']['table'] = 'field_data_field_western_today_image';
  $handler->display->display_options['fields']['field_western_today_image']['field'] = 'field_western_today_image';
  $handler->display->display_options['fields']['field_western_today_image']['label'] = '';
  $handler->display->display_options['fields']['field_western_today_image']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_western_today_image']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_western_today_image']['alter']['text'] = '<a href="[path]">[field_western_today_image]</a>';
  $handler->display->display_options['fields']['field_western_today_image']['alter']['path'] = '[path]';
  $handler->display->display_options['fields']['field_western_today_image']['alter']['preserve_tags'] = '<img> <a>';
  $handler->display->display_options['fields']['field_western_today_image']['element_type'] = '0';
  $handler->display->display_options['fields']['field_western_today_image']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_western_today_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_western_today_image']['element_wrapper_type'] = 'div';
  $handler->display->display_options['fields']['field_western_today_image']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_western_today_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_western_today_image']['settings'] = array(
    'image_style' => 'flexslider_full',
    'image_link' => 'content',
  );
  $handler->display->display_options['fields']['field_western_today_image']['delta_limit'] = '1';
  $handler->display->display_options['fields']['field_western_today_image']['delta_offset'] = '0';
  /* Field: Content: Video */
  $handler->display->display_options['fields']['field_westerntv_video']['id'] = 'field_westerntv_video';
  $handler->display->display_options['fields']['field_westerntv_video']['table'] = 'field_data_field_westerntv_video';
  $handler->display->display_options['fields']['field_westerntv_video']['field'] = 'field_westerntv_video';
  $handler->display->display_options['fields']['field_westerntv_video']['label'] = '';
  $handler->display->display_options['fields']['field_westerntv_video']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_westerntv_video']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_westerntv_video']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_westerntv_video']['type'] = 'file_rendered';
  $handler->display->display_options['fields']['field_westerntv_video']['settings'] = array(
    'file_view_mode' => 'slideshow',
  );
  $handler->display->display_options['fields']['field_westerntv_video']['delta_offset'] = '0';
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '[field_western_today_image][field_westerntv_video]';
  $handler->display->display_options['fields']['nothing']['alter']['path'] = '[path]';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;

  /* Display: Landing Page */
  $handler = $view->new_display('panel_pane', 'Landing Page', 'panel_pane_2');

  /* Display: Featured Stories RSS */
  $handler = $view->new_display('feed', 'Featured Stories RSS', 'feed_1');
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['style_plugin'] = 'rss';
  $handler->display->display_options['row_plugin'] = 'node_rss';
  $handler->display->display_options['path'] = 'featured-stories/rss';

  /* Display: Featured Stories */
  $handler = $view->new_display('panel_pane', 'Featured Stories', 'panel_pane_3');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Featured Stories';
  $handler->display->display_options['defaults']['css_class'] = FALSE;
  $handler->display->display_options['css_class'] = 'featured-stories';
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '4';
  $handler->display->display_options['pager']['options']['offset'] = '1';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_western_today_image']['id'] = 'field_western_today_image';
  $handler->display->display_options['fields']['field_western_today_image']['table'] = 'field_data_field_western_today_image';
  $handler->display->display_options['fields']['field_western_today_image']['field'] = 'field_western_today_image';
  $handler->display->display_options['fields']['field_western_today_image']['label'] = '';
  $handler->display->display_options['fields']['field_western_today_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_western_today_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_western_today_image']['settings'] = array(
    'image_style' => 'featured_story_preview',
    'image_link' => 'content',
  );
  $handler->display->display_options['fields']['field_western_today_image']['delta_limit'] = '1';
  $handler->display->display_options['fields']['field_western_today_image']['delta_offset'] = '0';
  /* Field: Content: Type */
  $handler->display->display_options['fields']['type']['id'] = 'type';
  $handler->display->display_options['fields']['type']['table'] = 'node';
  $handler->display->display_options['fields']['type']['field'] = 'type';
  $handler->display->display_options['fields']['type']['label'] = '';
  $handler->display->display_options['fields']['type']['element_label_colon'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_type'] = 'h2';
  $handler->display->display_options['fields']['title']['element_class'] = 'helper-clear-float';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['body']['alter']['max_length'] = '100';
  $handler->display->display_options['fields']['body']['alter']['trim'] = TRUE;
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['settings'] = array(
    'trim_length' => '500',
  );

  /* Display: First Featured Story */
  $handler = $view->new_display('panel_pane', 'First Featured Story', 'panel_pane_4');
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '1';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_western_today_image']['id'] = 'field_western_today_image';
  $handler->display->display_options['fields']['field_western_today_image']['table'] = 'field_data_field_western_today_image';
  $handler->display->display_options['fields']['field_western_today_image']['field'] = 'field_western_today_image';
  $handler->display->display_options['fields']['field_western_today_image']['label'] = '';
  $handler->display->display_options['fields']['field_western_today_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_western_today_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_western_today_image']['settings'] = array(
    'image_style' => 'first_featured_story',
    'image_link' => 'content',
  );
  $handler->display->display_options['fields']['field_western_today_image']['delta_limit'] = '1';
  $handler->display->display_options['fields']['field_western_today_image']['delta_offset'] = '0';
  /* Field: Content: Video */
  $handler->display->display_options['fields']['field_westerntv_video']['id'] = 'field_westerntv_video';
  $handler->display->display_options['fields']['field_westerntv_video']['table'] = 'field_data_field_westerntv_video';
  $handler->display->display_options['fields']['field_westerntv_video']['field'] = 'field_westerntv_video';
  $handler->display->display_options['fields']['field_westerntv_video']['label'] = '';
  $handler->display->display_options['fields']['field_westerntv_video']['element_class'] = 'float-right';
  $handler->display->display_options['fields']['field_westerntv_video']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_westerntv_video']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_westerntv_video']['type'] = 'file_rendered';
  $handler->display->display_options['fields']['field_westerntv_video']['settings'] = array(
    'file_view_mode' => 'preview',
  );
  $handler->display->display_options['fields']['field_westerntv_video']['delta_offset'] = '0';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_type'] = 'h2';
  $handler->display->display_options['fields']['title']['element_class'] = 'helper-clear-float';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: Content: Author */
  $handler->display->display_options['fields']['field_author']['id'] = 'field_author';
  $handler->display->display_options['fields']['field_author']['table'] = 'field_data_field_author';
  $handler->display->display_options['fields']['field_author']['field'] = 'field_author';
  $handler->display->display_options['fields']['field_author']['label'] = '';
  $handler->display->display_options['fields']['field_author']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_author']['empty'] = 'by Western Today Staff';
  $export['features'] = $view;

  return $export;
}
