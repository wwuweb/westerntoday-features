<?php
/**
 * @file
 * mailchimp_newsletter.features.inc
 */

/**
 * Implements hook_default_mailchimp_signup().
 */
function mailchimp_newsletter_default_mailchimp_signup() {
  $items = array();
  $items['get_our_newsletter'] = entity_import('mailchimp_signup', '{
    "name" : "get_our_newsletter",
    "mc_lists" : { "de50030caf" : "de50030caf" },
    "mode" : "1",
    "title" : "Get Our Newsletter",
    "settings" : {
      "path" : "",
      "submit_button" : "Subscribe",
      "confirmation_message" : "Thanks for subscribing!",
      "destination" : "",
      "mergefields" : {
        "EMAIL" : {
          "name" : "Email Address",
          "req" : true,
          "field_type" : "email",
          "public" : true,
          "show" : true,
          "order" : "1",
          "default" : null,
          "helptext" : null,
          "size" : "25",
          "tag" : "EMAIL",
          "id" : 0
        },
        "FNAME" : 0,
        "LNAME" : 0
      },
      "description" : "Want to stay on top of what\\u0027s happening at Western Washington University?  Sign up below to start receiving our daily email.  Unsubscribe at any time.  ",
      "doublein" : 0,
      "send_welcome" : 1,
      "include_interest_groups" : 0
    },
    "rdf_mapping" : []
  }');
  return $items;
}
