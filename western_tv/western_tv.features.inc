<?php
/**
 * @file
 * western_tv.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function western_tv_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "file_entity" && $api == "file_default_displays") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function western_tv_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function western_tv_node_info() {
  $items = array(
    'western_tv' => array(
      'name' => t('Western TV'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
